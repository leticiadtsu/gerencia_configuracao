package gerencia_configuracao;

/**
 *
 * @author Leticia
 */
public class ClasseA {
    private int a1;
    private float a2;
    private String a3;
    private String Aprinc4;
    private String Aramo4;

    public int getA1() {
        return a1;
    }

    public float getA2() {
        return a2;
    }

    public void setA1(int a1) {
        this.a1 = a1;
    }

    public void setA2(float a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getAprinc4() {
        return Aprinc4;
    }

    public void setAprinc4(String Aprinc4) {
        this.Aprinc4 = Aprinc4;
    }
        
    public String getAramo4() {
        return Aramo4;
    }

    public void setAramo4(String Aramo4) {
        this.Aramo4 = Aramo4;

    }
    
    public float soma (int x, float y){
        return x+y;
    }
    
}
